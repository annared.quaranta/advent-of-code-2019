filepath = 'input.txt'
with open(filepath) as fp:
  array = [int(x) for x in next(fp).split(',')]
  array = array[:]
  array[1] = 12
  array[2] = 2
  for k in range(0, len(array), 4):
    if array[k] == 99:
        break
    elif array[k] == 1:
        array[array[k+3]] = array[array[k+1]]+array[array[k+2]]
    elif array[k] == 2:
        array[array[k+3]] = array[array[k+1]]*array[array[k+2]]
    
  print(array[0])
