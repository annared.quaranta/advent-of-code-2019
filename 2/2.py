
def part1(array):
  array = array[:]
  for k in range(0, len(array), 4):
    if array[k] == 99:
        break
    elif array[k] == 1:
        array[array[k+3]] = array[array[k+1]]+array[array[k+2]]
    elif array[k] == 2:
        array[array[k+3]] = array[array[k+1]]*array[array[k+2]]
  return array[0]


filepath = 'input.txt'
with open(filepath) as fp:
  array = [int(x) for x in next(fp).split(',')]
  for noun in range(100):
    for verb in range(100):
      array[1] = noun
      array[2] = verb
      result = part1(array)
      if result == 19690720:
        print(100 * noun + verb)
        break