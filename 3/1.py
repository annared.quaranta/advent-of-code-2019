def path(wire):
  lista = set()
  x = 0
  y = 0
  for direction in wire:
    m = direction[1:]
    move = int(m);
    if (direction[:1] == 'R'):
      for a in range(0, move):
        lista.add((x+a, y))
      x += move
    elif (direction[:1] == 'L'):
      for a in range(0, move):
        lista.add((x-a, y))
      x -= move
    elif (direction[:1] == 'U'):
      for a in range(0, move):
        lista.add((x, y+a))
      y += move
    elif (direction[:1] == 'D'):
      for a in range(0, move):
        lista.add((x, y-a))
      y -= move  
  return lista

filepath = 'input.txt'
with open(filepath) as fp:
  wire1 = fp.readline().rstrip('\n').split(',')
  wire2 = fp.readline().rstrip('\n').split(',')
  path1 = path(wire1);
  path2 = path(wire2);
  intersection = (path1 & path2)
  intersection.remove((0,0))
  distances = [(abs(i[0])+abs(i[1])) for i in intersection]
  print(min(distances))
  