def path(wire):
  lista = set()
  lista1 = [] 
  x = 0
  y = 0
  steps = 0;
  for direction in wire:
    m = direction[1:]
    move = int(m);
    if (direction[:1] == 'R'):
      for a in range(0, move):
        lista.add((x+a, y))
        lista1.append({(x+a, y): steps})
        steps+=1
      x += move
    elif (direction[:1] == 'L'):
      for a in range(0, move):
        lista.add((x-a, y))
        lista1.append({(x-a, y): steps})
        steps+=1
      x -= move
    elif (direction[:1] == 'U'):
      for a in range(0, move):
        lista.add((x, y+a))
        lista1.append({(x, y+a): steps})
        steps+=1
      y += move
    elif (direction[:1] == 'D'):
      for a in range(0, move):
        lista.add((x, y-a))
        lista1.append({(x, y-a): steps})
        steps+=1
      y -= move  
  return [lista, lista1]

filepath = 'input.txt'
with open(filepath) as fp:
  wire1 = fp.readline().rstrip('\n').split(',')
  wire2 = fp.readline().rstrip('\n').split(',')
  path1 = path(wire1);
  path2 = path(wire2);
  intersection = (path1[0] & path2[0])
  intersection.remove((0,0))
  somma = []
  for i in intersection:
    value1 = [p.get(i) for p in path1[1] if isinstance(p.get(i), int)]
    value2 = [p.get(i) for p in path2[1] if isinstance(p.get(i), int)]
    somma.append(value1[0] + value2[0])
  print(min(somma))
  

