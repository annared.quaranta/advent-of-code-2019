def getFuel(mass, result):
  fuel = (int(mass)//3)-2
  if fuel > 0:
    result += fuel
    return getFuel(fuel, result)
  else:
    return result

filepath = 'input.txt'
with open(filepath) as fp:
  line = fp.readline()
  result = 0
  while line:
    if line.strip():
      result = getFuel(line, result);
      line = fp.readline()
  print (result)



