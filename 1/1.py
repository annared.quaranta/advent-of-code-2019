filepath = 'input.txt'
with open(filepath) as fp:
  line = fp.readline()
  result = 0
  while line:
    if line.strip():
      result += (int(line)//3)-2
      line = fp.readline()
  print (result)
    