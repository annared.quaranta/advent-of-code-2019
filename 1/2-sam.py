filepath = 'input.txt'
with open(filepath) as fp:
  line = fp.readline()
  result = 0
  while line:
    if line.strip():
      fuel = (int(line)//3)-2
      line = fp.readline()
      result += fuel;
      remainingMass = fuel
      while True:
        fuelForFuel = (int(remainingMass)//3)-2
        if fuelForFuel <= 0:
          break
        result += fuelForFuel
        remainingMass = fuelForFuel
  print (result)