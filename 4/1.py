minValue = 245182
maxValue = 790572

counter = 0
for i in range(minValue, maxValue):
	digits = map(int, str(i))
	if len(digits) <= 6:
		if (digits == sorted(digits) and any(digits[d] == digits[d+1] for d in range(len(digits)-1))):
			counter += 1

print(counter)